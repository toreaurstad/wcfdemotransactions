﻿using System;
using System.Linq;
using System.Collections.Generic;
using WcfTransactionsDemo.Common.DataContract;
using WcfTransactionsDemo.Common.ServiceContract;
using WcfTransactionsDemo.EntityFramework;
using System.ServiceModel;
using System.Transactions;

namespace WcfTransactionsDemo.ServiceImplementation
{

    [ServiceBehavior(TransactionIsolationLevel = IsolationLevel.Serializable, TransactionTimeout = "00:00:30")]
    public class SampleServiceImplementation : ISampleServiceContract
    {

        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        public string AdjustBalance(int customerId, decimal amount)
        {
            try
            {
                using (var dbContext = new EntityFramework.TransactionsDemoEntities())
                {
                    Customer customer = dbContext.Customers.Single(c => c.CustomerId == customerId);
                    customer.Balance += amount;
                    dbContext.SaveChanges();
                    return string.Format("The balance of customer {0} was updated to {1}", customer.Name, customer.Balance);
                }
            }
            catch (Exception err)
            {
                throw new FaultException<FaultDataContract>(new FaultDataContract { Message = err.Message });
            }
        }

        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        public string AdjustInventory(int productId, int quantity)
        {
            try
            {
                using (var dbContext = new EntityFramework.TransactionsDemoEntities())
                {
                    Product product = dbContext.Products.Single(p => p.ProductId == productId);
                    product.InStock += quantity;
                    dbContext.SaveChanges();
                    return string.Format("The on hand of product {0} was updated to {1}", product.ProductName, product.InStock);
                }
            }
            catch (Exception err)
            {
                throw new FaultException<FaultDataContract>(new FaultDataContract { Message = err.Message });
            }
        }

        public List<CustomerDataContract> GetAllCustomers()
        {
            using (var dbContext = new EntityFramework.TransactionsDemoEntities())
            {
                var customers = dbContext.Customers.Select(c => new CustomerDataContract
                {
                   CustomerId = c.CustomerId,
                   Balance = c.Balance,
                   Customer = c.Name
                });
                return customers.ToList();
            }
        }

        public List<ProductDataContract> GetAllProducts()
        {
            using (var dbContext = new EntityFramework.TransactionsDemoEntities())
            {
                var products = dbContext.Products.Select(p => new ProductDataContract
                {
                    Name = p.ProductName,
                    Price = p.Price,
                    OnHand = p.InStock,
                    ProductId = p.ProductId
                });
                return products.ToList();
            }
            
        }

        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        public string PlaceOrder(OrderDataContract order)
        {
            return string.Format(@"Placed and order of product {0} from customer {1} with qty {2}", order.ProductId, order.CustomerId, order.Quantity);
        }
    }
}
