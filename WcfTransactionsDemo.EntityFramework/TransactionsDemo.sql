﻿
CREATE DATABASE TransactionsDemo
 
use TransactionsDemo
go 

IF OBJECT_ID('dbo.Customer') IS NOT NULL
 DROP TABLE Customer 

IF OBJECT_ID('dbo.Products') IS NOT NULL
 DROP TABLE Products 

CREATE TABLE Customer(
CustomerId INT PRIMARY KEY IDENTITY(1,1),
Name NVARCHAR(100),
Balance DECIMAL)

INSERT INTO Customer VALUES('Contoso', 10000.00)
INSERT INTO Customer VALUES('Northwind', 25000.00)
INSERT INTO Customer VALUES('Litware', 50000.00)


CREATE TABLE Products(
ProductId INT PRIMARY KEY IDENTITY(1,1),
ProductName NVARCHAR(100),
Price DECIMAL,
InStock INT)

INSERT INTO Products VALUES('Wood', 100.00, 990)
INSERT INTO Products VALUES('Wallboard', 200.00, 2500)
INSERT INTO Products VALUES('Pipe', 500.00, 5000)
