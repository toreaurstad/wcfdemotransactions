﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows;
using WcfTransactionsDemo.Client.SampleService;

namespace WcfTransactionsDemo.Client
{
    public class MainWindowViewModel
    {

        public MainWindowViewModel()
        {
            Customers = new ObservableCollection<CustomerDataContract>();
            Products = new ObservableCollection<ProductDataContract>();

            LoadCustomers();
            LoadProducts();

            PlaceOrderCommand = new RelayCommand<object>(PlaceOrderCommandExecute);
        }

        private void PlaceOrderCommandExecute(object obj)
        {
            using (var client = new SampleServiceContractClient())
            {

                using (var transactionScope = new TransactionScope())
                {
                    try
                    {


                        string orderPlacement = client.PlaceOrder(new OrderDataContract
                        {
                            CustomerId = SelectedCustomer != null ? SelectedCustomer.CustomerId : 0,
                            ProductId = SelectedProduct != null ? SelectedProduct.ProductId : 0,
                            Quantity = Quantity
                        });
                        MessageBox.Show("Placing order: " + orderPlacement);



                        string adjustedInventory = client.AdjustInventory(SelectedProduct != null ?
                            SelectedProduct.ProductId : 0, -1 * Quantity);

                        MessageBox.Show("Adjusting inventory: " + adjustedInventory);


                        string adjustedBalance = client.AdjustBalance(SelectedCustomer != null ?
                            SelectedCustomer.CustomerId : 0, -1 * (SelectedProduct != null && SelectedProduct.Price > 0 ? SelectedProduct.Price.Value : 0) * Quantity);

                        MessageBox.Show("Adjusting balance: " + adjustedBalance);

                        transactionScope.Complete(); 

                    }
                    catch (FaultException err)
                    {
                        MessageBox.Show("FaultException: " + err.Message);
                    }
                    catch (ProtocolException perr)
                    {
                        MessageBox.Show("ProtocolException: " + perr.Message);
                    }
                }

                try
                {
                    LoadCustomers();
                    LoadProducts();
                }
                catch (Exception err)
                {
                    Console.WriteLine(err.Message);
                }
            }
        }

        private void LoadProducts()
        {
            Products.Clear(); 
            using (var client = new SampleServiceContractClient())
            {
                var products = client.GetAllProducts(); 
                if (products != null)
                {
                    foreach (var product in products)
                    {
                        Products.Add(product);
                    }
                }
            }
        }

        private void LoadCustomers()
        {
            Customers.Clear();
            using (var client = new SampleServiceContractClient())
            {
                var customers = client.GetAllCustomers();
                if (customers != null)
                {
                    foreach (var customer in customers)
                    {
                        Customers.Add(customer);
                    }
                }
            }
        }

        public ObservableCollection<CustomerDataContract> Customers { get; private set; }

        public ObservableCollection<ProductDataContract> Products { get; private set; }

        public CustomerDataContract SelectedCustomer { get; set; }

        public ProductDataContract SelectedProduct { get; set; }

        public RelayCommand<object> PlaceOrderCommand { get; set; }

        public int Quantity { get; set; }


    }
}
