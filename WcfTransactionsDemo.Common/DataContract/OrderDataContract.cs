﻿using System.Runtime.Serialization;

namespace WcfTransactionsDemo.Common.DataContract
{

    [DataContract(Namespace =Constants.DataContractsNamespace)]
    public class OrderDataContract
    {

        [DataMember(Order = 1)]
        public int ProductId { get; set; }

        [DataMember(Order = 2)]
        public int Quantity { get; set; }

        [DataMember(Order = 3)]
        public int CustomerId { get; set; }

    }
}
