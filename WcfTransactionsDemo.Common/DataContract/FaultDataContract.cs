﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace WcfTransactionsDemo.Common.DataContract
{

    [DataContract(Namespace =Constants.DataContractsNamespace)]
    public class FaultDataContract
    {

        [DataMember(Order = 1)]
        public string Message { get; set; }


    }

}
