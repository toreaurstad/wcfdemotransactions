﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace WcfTransactionsDemo.Common.DataContract
{

    [DataContract(Namespace =Constants.DataContractsNamespace)]
    public class CustomerDataContract
    {
        [DataMember(Order = 1)]
        public int CustomerId { get; set; }

        [DataMember(Order = 2)]
        public string Customer { get; set; }

        [DataMember(Order = 3)]
        public decimal? Balance { get; set; }
    }
}
