﻿using System.Runtime.Serialization;

namespace WcfTransactionsDemo.Common.DataContract
{

    [DataContract(Namespace =Constants.DataContractsNamespace)]
    public class ProductDataContract
    {

        [DataMember(Order = 1)]
        public int ProductId { get; set; }

        [DataMember(Order = 2)]
        public string Name { get; set; }

        [DataMember(Order = 3)]
        public decimal? Price { get; set; }

        [DataMember(Order = 4)]
        public int? OnHand { get; set; }

    }

}
