﻿namespace WcfTransactionsDemo.Common
{

    public class Constants
    {

        public const string DataContractsNamespace = "http://schemas.acmeindustries.org/DataContracts/2018/07";

        public const string ServiceContractsNamespace = "http://schemas.acmeindustries.org/ServiceContracts/2018/07";

    }

}
